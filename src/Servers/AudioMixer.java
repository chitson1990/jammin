package Servers;

import java.io.DataOutputStream;

import Tools.AudioChunk;
import Tools.HardCodedValues;

//TODO
public class AudioMixer implements Runnable {

	private Server server;
	private DataOutputStream dataOutCanal;
	private AudioChunk audio = null;
	private boolean canMixData = false;

	public AudioMixer(Server server, DataOutputStream dataOutCanal) {
		this.server = server;
		this.dataOutCanal = dataOutCanal;
	}

	public void setData(AudioChunk audio) {
		this.audio = audio;
		canMixData = true;
	}

	@Override
	public void run() {
		while (true) {
			HardCodedValues.sleep(1);
			synchronized (this) {
				if (canMixData) {
					canMixData = false;
					if (!server.mixAudioExceptMine(dataOutCanal, audio)) {
						break;
					}
				}
			}
		}
	}
}
